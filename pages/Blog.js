import React from "react";
import TopHeader from "./TopHeader";
import BlogHeader from "../components/BlogHeader";
import NewsList from "../components/NewsList";
import BlogList from "../components/BlogList";

const Blog = () => {
  return (
    <>
      <TopHeader />
      <BlogHeader />
      <BlogList />
    </>
  );
};

export default Blog;
