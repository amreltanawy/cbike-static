import React from "react";
import TopHeader from "./TopHeader";
import styles from "../styles/HowToRent.module.scss";
import { useRouter } from "next/router";
import HowToRentArabic from "./HowToRentArabic";
import en from "../components/locales/en";
import fr from "../components/locales/fr";

const HowToRent = () => {
  const router = useRouter();
  const { locale } = router;
  const t = locale === "en" ? en : fr;
  return locale === "en" ? (
    <div>
      <TopHeader />
      <div className={styles.pageContainer}>
        <div className={styles.pageIntroContainer}>
          <h1>You can take a Bike for more than a short ride</h1>
          <p>
            During your rental you can make stops, unlock and lock your bike as
            many times as you want. At the end of your rental bring the Donkey
            to a drop-off location.
          </p>
        </div>
        <div className={styles.howItWorks}>
          <h1>How it works</h1>
          <div className={styles.hiwRow}>
            <img src="/images/Mockup-02.png" alt="" width="600" />
            <div className={styles.hiwRowText}>
              <h2>Rent a bike with a few clicks</h2>
              <p>
                Open the Cairo Bike app to see rental locations, find a Donkey
                near you and rent it with your phone.
              </p>
            </div>
          </div>
          <div className={styles.hiwRow}>
            <div className={styles.hiwRowText}>
              <h2>Unlock with your phone</h2>
              <p>
                Connect to the lock via Bluetooth to lock and unlock your Cairo
                Bike whenever you make a stop.
              </p>
            </div>
            <img src="/images/Mockup-02.png" alt="" width="600" />
          </div>
          <div className={styles.hiwRow + " diff-row"}>
            <img src="/images/Mockup-02.png" alt="" width="600" />
            <div className={styles.hiwRowText}>
              <h2>End your rental at a drop-off location</h2>
              <p>
                When you are done riding, bring the Cairo Bike to an available
                drop-off location, which are shown with dark pins on the map.
              </p>
            </div>
          </div>
        </div>

        <div className={styles.dlTheApp}>
          <h3>Download the app to rent a bike today</h3>
          <a href="http://cbikeapp.herokuapp.com/">
            <button className="btn btn-warning">Get the app</button>
          </a>
        </div>
      </div>
    </div>
  ) : (
    <HowToRentArabic />
  );
};

export default HowToRent;
