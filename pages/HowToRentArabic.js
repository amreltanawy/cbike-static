import React from "react";
import TopHeader from "./TopHeader";
import styles from "../styles/HowToRent.module.scss";
import { useRouter } from "next/router";
import en from "../components/locales/en";
import fr from "../components/locales/fr";

const HowToRentArabic = () => {
  const router = useRouter();
  const { locale } = router;
  const t = locale === "en" ? en : fr;
  return (
    <div>
      <TopHeader />
      <div className={styles.pageContainer}>
        <div
          className={styles.pageIntroContainer}
          style={{ textAlign: "right" }}
        >
          <h1>استأجر كايرو بايك ببضع نقرات</h1>
          <p>
            افتح تطبيق كايرو بايك لمعرفة مواقع التأجير، وابحث عن دراجة كايرو
            بايك بالقرب منك واستأجرها بهاتفك.
          </p>
        </div>
        <div className={styles.Arabic}>
          <div className={styles.howItWorks}>
            <h1>كيف تعمل</h1>
            <div className={styles.hiwRow}>
              <img src="/images/Mockup-02.png" alt="" width="600" />
              <div className={styles.hiwRowText}>
                <h2>استأجر كايرو بايك ببضع نقرات</h2>
                <p>
                  افتح تطبيق كايرو بايك لمعرفة مواقع التأجير، وابحث عن دراجة
                  كايرو بايك بالقرب منك واستأجرها بهاتفك.
                </p>
              </div>
            </div>
            <div className={styles.hiwRow}>
              <div className={styles.hiwRowText}>
                <h2>افتح مع هاتفك</h2>
                <p>
                  قم بالاتصال بالقفل عبر البلوتوث لقفل وفتح دراجتك الهوائية في
                  كايرو بايك متى توقفت
                </p>
              </div>
              <img src="/images/Mockup-02.png" alt="" width="600" />
            </div>
            <div className={styles.hiwRow}>
              <img src="/images/Mockup-02.png" alt="" width="600" />
              <div className={styles.hiwRowText}>
                <h2>قم بإنهاء إيجارك في موقع التسليم</h2>
                <p>
                  عند الانتهاء من الركوب، أحضر كايرو بايك إلى موقع التسليم
                  المتاح ، والذي يظهر مع دبابيس داكنة على الخريطة
                </p>
              </div>
            </div>
          </div>
          {/* <div className={styles.dlTheApp}>
            <h3> قم بتنزيل التطبيق لاستئجار دراجة اليوم</h3>
            <a href="">
              <button className="btn btn-warning">Get the app</button>
            </a>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default HowToRentArabic;
