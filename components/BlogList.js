import React, { useEffect, useState } from "react";
import Footer from "../components/Footer";
import axios from "axios";
import ReactReadMoreReadLess from "react-read-more-read-less";
import BikeBlog from "./BikeBlog";
import { useRouter } from "next/router";

const BlogList = () => {
  const router = useRouter();
  const { blog } = router.query;
  const { locale } = router;
  const isEnglish = locale === "en";
  const [blogs, setBlogs] = useState(
    isEnglish
      ? [
          {
            link: "https://sis.gov.eg/Story/170183/Cairo-governorate-gears-up-for-COP27-via-launching-'Cairo-Bike'-project?lang=en-us",
            image: "/images/English Blog 1.jpg",
            title:
              "Cairo governorate gears up for COP27 via launching 'Cairo Bike' project",

            description:
              "Cairo governorate started to set up five stops for 'Cairo Bike' project at Tahrir, Mohamed Farid and Abdel Moneim Riyad squares and El-Falaki and Abdel-Khaleq Tharwat streets in Downtown Cairo",
          },
          {
            link: "https://www.egypttoday.com/Article/1/117889/Know-more-about-Cairo-Bike-project-Egypt-s-bid-for",
            image: "/images/English Blog 2.jpg",
            title:
              "Know more about Cairo Bike project: Egypt's bid for less traffic congestion and higher air quality",

            description:
              "Before hosting the United Nations Conference of Parties on Climate Change at its twenty-seventh session, scheduled to be held in Sharm El-Sheikh next November, Egypt began implementing the “Cairo Bike” project to spread the culture of cycling, in a bid of enhancing the concept of environment friendly transportation services.",
          },
          {
            link: "https://www.al-monitor.com/originals/2022/07/egyptian-government-pushes-bike-project-reduce-traffic-pollution",
            image: "/images/English Blog 3.jpg",
            title:
              "Egyptian government pushes bike project to reduce traffic, pollution.",
            description:
              "In lead-up to COP27, the Egyptian government is exploring ways to encourage citizens to ride bicycles.",
          },
          {
            link: "https://www.faydety.com/en/learn/fun-facts-4/cairo-bike-1-egp-hour-8-egp-day-2334",
            image: "/images/English Blog 4.jpeg",
            title: "Cairo Bike: 1 EGP/hour & 8 EGP/day",

            description:
              "As the trial launch of the Cairo Bike project starts Faydety is here to tell you all about Cairo Bike, where to find them, how to rent one, and how much it costs.",
          },
        ]
      : [
          {
            link: "https://al-ain.com/article/cairo-bike-projecegypt",
            image: "/images/Arabic Blog 1.jpg",
            title: "مصر ترجئ تنفيذ مشروع كايرو بايك.. أرخص وسيلة مواصلات",
            description:
              "أرجأت مصر تنفيذ مشروع ' كايرو بايك'، الذي يقوم على استخدام الدراجات كوسيلة للتنقل بالقاهرة عبر تطبيق على الموبايل وبأسعار مخفضة",
          },
          {
            link: "https://akhbarelyom.com/news/newdetails/3826295/1/%D8%A7%D9%84%D8%B3%D8%A7%D8%B9%D8%A9-%D8%A8%D8%AC%D9%86%D9%8A%D9%87-%D9%83%D9%84-%D9%85%D8%A7-%D8%AA%D8%B1%D9%8A%D8%AF-%D9%85%D8%B9%D8%B1%D9%81%D8%AA%D9%87-%D8%B9%D9%86-%D9%85%D8%B4%D8%B1%D9%88%D8%B9-",
            image: "/images/Arabic Blog 2.jpg",
            title:
              " الساعة بجنيه.. كل ما تريد معرفته عن مشروع الدراجات الجديدة بوسط البلد",
            description:
              "أطلقت محافظة القاهرة مؤخراً مشروع « كايرو بايك» في أكثر من 5 محطات بوسط البلد، بميادين التحرير ومحمد فريد وعبد المنعم رياض وشارعي الفلكي وعبد الخالق ثروت، استعدادًا لبدء التشغيل التجريبي ضمن الإجراءات التنفيذية لتفعيل المشروع            ",
          },
          {
            link: "https://akhbarelyom.com/news/newdetails/3825860/1/%D9%85%D8%AD%D8%A7%D9%81%D8%B8-%D8%A7%D9%84%D9%82%D8%A7%D9%87%D8%B1%D8%A9-%D9%85%D8%B4%D8%B1%D9%88%D8%B9-%D9%83%D8%A7%D9%8A%D8%B1%D9%88-%D8%A8%D8%A7%D9%8A%D9%83-%D9%8A%D8%A3%D8%AA%D9%8A-%D9%85%D8%AA%D9%88%D8%A7            ",
            image: "/images/Arabic Blog 5.jpg",
            title:
              " محافظ القاهرة: مشروع «كايرو بايك» يأتي متواكبًا مع استضافة قمة المناخ",
            description:
              "بدأت محافظة القاهرة في إقامة ٥ محطات لمشروع كايرو بايك بميادين التحرير ومحمد فريد وعبد المنعم رياض وشارعي الفلكي وعبد الخالق ثروت.",
          },
        ]
  );

  console.log("blog", blog);
  // useEffect(() => {
  //   if (blog) {
  //     fetchBlog()
  //   }
  // }, [blog])

  // const fetchBlog = async () => {
  //   try {
  //     const res = await axios.get(`https://3jj2zsfcm6.execute-api.us-east-1.amazonaws.com/dev/api/getBlogs?categoryId=${blog}`)
  //     console.log("blog res", res.data.data);

  //     setBlogs(res.data.data)
  //   } catch (err) {
  //     console.log(err);
  //   }
  // }
  return (
    <>
      <div className="blog__list container">
        <div className="row">
          {
            blogs?.map((blog, i) => {
              return (
                <BikeBlog
                  key={i}
                  image={blog.image}
                  title={blog.title}
                  description={blog.description}
                  date={blog.date}
                  author={blog.author}
                  link={blog.link}
                />
              );
            })
            // <BikeBlog image={blogs.image} title={blogs.title} description={blogs.description} />
          }
        </div>
      </div>
      <Footer />
    </>
  );
};

export default BlogList;
