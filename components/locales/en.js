export default {
  header: {
    rent: "Rent a bike in Cairo",
    unlock: "Unlock and ride a bike using your Subscription in Downtown and Zamalek (soon).",
    reach: "Subscribe Now",
  },
  work: {
    howWork: "How it works",
    rentABike: {
      rentaBike: "Rent A Bike",
      location:
        "Open the Donkey Republic app to see pick-up locations,and rent it with your phone",
    },
    unlockPhone: {
      unlock: "Unlock with your phone",
      connectBluetooth:
        "Connect to the lock via Bluetooth to lock and unlock your Donkey whenever you make a stop",
    },
    ride: {
      rideBike: "Ride and keep the bike",
      rentals:
        "Short or long rentals.Lock and unlock your bikeas much as you like",
    },
    return: {
      returnEnd: "Return at the end",
      rideAvailable:
        "the Donkey to an available drop-off ocation, end your rental in the appWhen you are done riding, bring",
    },
  },
  faq: {
    question: "Questions?",
    accordianHeading: "Dropdown",
    accordianItem1:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.",
    accordianHeading2: "Dropdown",
    accordianItem2:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.",
    accordianHeading3: "Dropdown",
    accordianItem3:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique. Duis cursus, mi quis viverra ornare, eros dolor interdum nulla, ut commodo diam libero vitae erat. Aenean faucibus nibh et justo cursus id rutrum lorem imperdiet. Nunc ut sem vitae risus tristique posuere.",
  },
  faqs: [
    {
      question: "How does the Daily Rider membership work?",
      answer: [
        "Each time you rent a Donkey with an active Daily Rider membership, you get the first 2 hours per day at no extra cost. These 2 hours can be used for multiple rentals. If you choose to keep the same Donkey on the current rental for longer than 2 hours, then please be aware that a discounted rate is charged for any additional time, per rental per day. This means that your rental automatically extends at a fixed low cost when your included free time expires.",
        "The monthly fee and the price per rental per day can be found in the app and will be charged in your local currency.",
        "Please note that this membership only applies for non-electric bikes. The membership can be used in all 50+ Donkey cities and it also includes theft insurance.",
        "The membership renews automatically each month and can be cancelled at any time in the membership section of the app when you have chosen the 1 month membership plan.",
        "If you choose the 1 month membership, where you can cancel anytime, a set up fee is included and is non-refundable. This is different from the 3 month membership plan, where there is a binding period of 3 month. Cancelling the membership will not immediately stop the membership, you will still be able to use your membership until the end of the prepaid period (30 days after the last payment, or in case of the 3 months minimum duration, at the end of these 3 months), even if you cancel your membership. ",
      ],
    },
    {
      question: "Cairo bike Memberships",
      answer: [
        "Cairo bike memberships are finally here!",
        "(Please note that the memberships will only be available in certain places, if these memberships are available you can find them while using the steps below)",
        "They work much like normal memberships but have a few different features. Here is a step-by-step guide on how to use them:",
        "1. Go to the become a member section.",
        "2. You can choose a bike membership there. ",
        "3. The memberships usually offer unlimited pedal rides, combined with a limited number of bike rides. This information is displayed at the top of the right-side menu, together with the usual wallet info, etc",
        "4. There is now a new extra information button (small ‘i’ to the top right of the right-side menu, as seen above), which indicates when the membership ends.",
      ],
    },
    // {
    //   question: "What are Cairo Bike memberships ?",
    //   answer: [
    //     "We have different memberships that give you access to an unlimited amount of rides for a certain amount of time per day.",
    //     // "There are two kinds of memberships: pedal-bike only memberships, and e-bike + pedal-bike memberships (availability depends on your city, please check the membership section of your app for details, and to see whether it is available in your city).",
    //     "Our memberships work globally, which means that if you sign up for a membership e.g. in Copenhagen, you can still use the benefits of the membership in the other cities where our service is available. Please note that the price and type of membership will be different from country to country and depending on the type of plan you choose. You can pay for it through any international payment card or PayPal.",
    //     "When you use one of our memberships, you will get a certain amount of minutes per day to be used. This means that you have a set amount of minutes you can use during the day no matter how many times you rent a Cairo Bike. When you go over this set amount of minutes, you will get a custom fixed price per hour for the extra time you use the bike. This price can differ depending on the membership you choose. (For details, please download our app, where you can see the pricing without needing to sign-up or sign-in.)",
    //   ],
    // },
    // {
    //   question: "How do I rent an e-bike?",
    //   answer: [
    //     "Install the Cairo Bike app for Android 5+ or iOS 11+.",
    //     "In the app, find the most convenient pick-up location for you, (the ones with an electric charge next to the number). This indicates an e-bike is available at this pick up location. ",
    //     "In the bike type selection, select E-Bike and the number of vehicles you would like to rent out (up to 5 vehicles per account) and tap on Continue.",
    //     "Please note that in order to use an e-bike, you must establish an online connection on your phone, and not just bluetooth. ",
    //     "Please note that you are able to rent multiple e-bikes, but only able to rent 1 type of vehicle. As such, you are not able to rent a bike and an e-bike in the same booking.",
    //     "You will be shown a screen with an overview of the total cost per vehicle. Scroll through the graph to see the price for a vehicle rental. If you wish to book a normal rental tap on Rent # vehicle.",
    //     "*Prices shown are only an indicator, for updated prices, please check in the app.",
    //     "Please note that you will have to pay separately for the e-bike if your membership does not include e-bikes in it. For more information on e-bike memberships, please see How do I sign up for a membership?",
    //     "After you have selected the option Rent # vehicles, choose Create account. If you already have an account, choose Login. You can decide whether to sign up through Facebook or by filling in your details.",
    //     "You will then be asked to add your phone number. Please select your country code, fill in the rest of the phone number and press Continue. You will receive a text message with a verification code. Once you have filled in the code, you will be able to proceed with the booking.",
    //     "If you are renting more than one vehicle, you will be shown the option to add your co-riders’ emails, so they will be able to lock and unlock the vehicle with their own app.",
    //     "Next, fill in your payment information and scroll down the page. You can then choose whether to add theft insurance or not. After allowing the processing of your personal data and accepting the Terms and Conditions of our service you will be able to press Rent Now.",
    //     "Locate the vehicle at the pick-up location, make sure that the name in the app matches the vehicle you are next to, tap Unlock in the app and saddle up - you're good to go!",
    //     "For information on how to rent a pedal bike, please click here. ",
    //   ],
    // },
  ],
  footer: {
    logoText: "Logo here",
    subscribeNow: "Subscribe Now",
    name: "Name",
    email: "E-mail",
    submitBtn: "Subscribe",
  },
  payment: {
    bundle: "Bundles",
    settler: "Settler",
    EGP600: "EGP600",
    year: "Per year",
    subscribe: "Subscribe",
    commuter: "Commuter",
    EGP100: "EGP100",
    month: "Per month",
    personal: "Personal",
    EGP8: "EGP8",
    day: "Per day",
    explorer: "Explorer",
    EGP1: "EGP1",
    hour: "Per hour",
  },
  register: {
    logoText: "Logo here",
    name: "Name",
    email: "Email",
    phoneNo: "Phone No",
    password: "Password",
    confirmPassword: "Confirm password",
    registerBtn: "Register",
    loginBtn: "Login",
    showPassword: "Show Password",
  },
  login: {
    logoText: "Logo here",
    email: "Email",
    password: "Password",
    loginBtn: "Login",
    showPassword: "Show Password",
  },
  navbar: {
    home: "Home",
    discover: "Blog",
    subDiscover: "Discover",
    events: "Events",
    news: "News",
    use: "How It Works",
    howRent: "How to Rent",
    price: "Prices",
    register: "Register",
    logoutBtn: "Logout",
  },
  language: {
    english: "🇬🇧",
    arabic: "🇪🇬",
  },
  googleMap: {
    station: "Find the nearest station",
  },
};
