import React, { Component, useEffect, useState } from "react";
// import GoogleMapReact from "google-map-react";
// import { GoogleApiWrapper, Marker, InfoWindow } from "google-maps-react";
import { useRouter } from "next/router";
import en from "./locales/en";
import fr from "./locales/fr";
// import GoogleMapArabic from "./GoogleMapArabic";
import axios from "axios";
// import CurrentMaker from "./CurrentMarker";
// import Markers from "./Markers";

let Map = (props) => <div>Loading ....</div>;

const GoogleMap = () => {
  const router = useRouter();
  const { locale } = router;
  const t = locale === "en" ? en : fr;
  const [longitude, setLongitude] = useState("");
  const [latitude, setLatitude] = useState("");
  const [coordinates, setCoordinates] = useState([]);

  const [fetchMapData, setFetchMapData] = useState([]);
  const [selectedElement, setSelectedElement] = useState(null);
  const [activeMarker, setActiveMarker] = useState(null);
  const [showInfoWindow, setInfoWindowFlag] = useState(true);

  let [isComponentMounted, setIsComponentMounted] = useState(false);
  useEffect(() => setIsComponentMounted(true), []);
  if (isComponentMounted) {
    Map = require("../components/Map").default;
  }

  const onMapClicked = async (clickEvent) => {
    setLongitude(clickEvent.lng);
    setLatitude(clickEvent.lat);
    // let payload = {
    //   longitude: clickEvent.lng,
    //   latitude: clickEvent.lat

    // }

    // const res = await axios.post("https://3jj2zsfcm6.execute-api.us-east-1.amazonaws.com/dev/api/save-map", payload)
  };

  useEffect(() => {
    const data = [{"id":1,"name":"ميدان عبد المنعم رياض مدخل السرفيس من الكورنيش","status":1,"location":{"type":"Point","coordinates":[31.234463,30.049978]},"textual_location":"ميدان عبد المنعم رياض مدخل السرفيس من الكورنيش","advertiser_info":null,"meta":null,"created_at":"2022-07-30T15:01:59.501872Z","updated_at":"2022-08-07T03:42:40.025537Z"},{"id":2,"name":"ميدان التحرير امام كنتاكي","status":1,"location":{"type":"Point","coordinates":[31.236358,30.044731]},"textual_location":"ميدان التحرير امام كنتاكي","advertiser_info":null,"meta":null,"created_at":"2022-07-30T15:04:14.377306Z","updated_at":"2022-08-07T03:44:51.711850Z"},{"id":5,"name":"داخل ممر الشواربي من قصر النيل – وسط البلد","status":1,"location":{"type":"Point","coordinates":[31.241261,30.048602]},"textual_location":"داخل ممر الشواربي من قصر النيل – وسط البلد","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:08:23.056526Z","updated_at":"2022-08-31T17:08:23.056544Z"},{"id":6,"name":"مدخل جراج التحرير من الميدان","status":1,"location":{"type":"Point","coordinates":[31.235066,30.044875]},"textual_location":"مدخل جراج التحرير من الميدان","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:11:59.971323Z","updated_at":"2022-08-31T17:11:59.971341Z"},{"id":7,"name":"امام ميدان عبد المنعم رياض - وسط البلد","status":1,"location":{"type":"Point","coordinates":[31.234716,30.048573]},"textual_location":"امام ميدان عبد المنعم رياض - وسط البلد","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:13:58.144762Z","updated_at":"2022-08-31T17:13:58.144783Z"},{"id":8,"name":"ميدان محمد فريد – امام محطة المترو- مقابل مسجد الطباخ وشرطة المرافق","status":1,"location":{"type":"Point","coordinates":[31.243845,30.045279]},"textual_location":"ميدان محمد فريد – امام محطة المترو- مقابل مسجد الطباخ وشرطة المرافق","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:16:00.211570Z","updated_at":"2022-08-31T17:16:00.211589Z"},{"id":9,"name":"تقاطع ش. الفلكي مع الشيخ ريحان","status":1,"location":{"type":"Point","coordinates":[31.239259,30.041661]},"textual_location":"تقاطع ش. الفلكي مع الشيخ ريحان","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:18:08.338672Z","updated_at":"2022-08-31T17:18:08.338691Z"},{"id":10,"name":"مدخل ش. محمود بسيوني من طلعت حرب","status":1,"location":{"type":"Point","coordinates":[31.238187,30.047807]},"textual_location":"مدخل ش. محمود بسيوني من طلعت حرب","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:20:04.945955Z","updated_at":"2022-08-31T17:20:04.945975Z"},{"id":11,"name":"ممر القاضي الفاضل بين قصر النيل ومحمد صبري ابوعلم","status":1,"location":{"type":"Point","coordinates":[31.240164,30.04728]},"textual_location":"ممر القاضي الفاضل بين قصر النيل ومحمد صبري ابوعلم","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:21:57.740694Z","updated_at":"2022-08-31T17:21:57.740715Z"},{"id":12,"name":"امام شركة التامين الاهلية تقاطع محمد فريد وقصر النيل- ميدان مصطفي كامل","status":1,"location":{"type":"Point","coordinates":[31.243925,30.049533]},"textual_location":"امام شركة التامين الاهلية تقاطع محمد فريد وقصر النيل- ميدان مصطفي كامل","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:23:08.727096Z","updated_at":"2022-08-31T17:23:08.727117Z"},{"id":13,"name":"ميدان لاظوغلي شارع نوبار رصيف شرطة السياحة","status":1,"location":{"type":"Point","coordinates":[31.24009,30.038587]},"textual_location":"ميدان لاظوغلي شارع نوبار رصيف شرطة السياحة","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:24:47.139905Z","updated_at":"2022-08-31T17:24:47.139922Z"},{"id":14,"name":"ناصية ش. الفلكي من ش. التحرير","status":1,"location":{"type":"Point","coordinates":[31.239835,30.044722]},"textual_location":"ناصية ش. الفلكي من ش. التحرير","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:26:07.233483Z","updated_at":"2022-08-31T17:26:07.233502Z"},{"id":15,"name":"الرصيف المقابل لنقابة المحامين ش. رمسيس","status":1,"location":{"type":"Point","coordinates":[31.237684,30.052543]},"textual_location":"الرصيف المقابل لنقابة المحامين ش. رمسيس","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:27:26.800000Z","updated_at":"2022-08-31T17:27:26.800019Z"},{"id":16,"name":"السور الخلفي لدار العلوم ناصية محمد عز العرب مع الشيخ علي يوسف","status":1,"location":{"type":"Point","coordinates":[31.235408,30.034973]},"textual_location":"السور الخلفي لدار العلوم ناصية محمد عز العرب مع الشيخ علي يوسف","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:28:35.353866Z","updated_at":"2022-08-31T17:28:35.353884Z"},{"id":17,"name":"أمام مستشفى الهلال شارع رمسيس","status":1,"location":{"type":"Point","coordinates":[31.243902,30.058581]},"textual_location":"أمام مستشفى الهلال شارع رمسيس","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:30:11.165368Z","updated_at":"2022-08-31T17:30:11.165388Z"},{"id":18,"name":"ش. رمسيس أمام نقابة التجاريين","status":1,"location":{"type":"Point","coordinates":[31.242174,30.056686]},"textual_location":"ش. رمسيس أمام نقابة التجاريين","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:32:03.461008Z","updated_at":"2022-08-31T17:32:03.461027Z"},{"id":19,"name":"يمين مخرج عبد الخالق ثروت المتجه لنفق الازهر ميدان الأوبرا","status":1,"location":{"type":"Point","coordinates":[31.246618,30.050179]},"textual_location":"يمين مخرج عبد الخالق ثروت المتجه لنفق الازهر ميدان الأوبرا","advertiser_info":{},"meta":{},"created_at":"2022-08-31T17:33:54.498819Z","updated_at":"2022-08-31T17:33:54.498843Z"},{"id":20,"name":"امام مدخل شارع الجمهورية","status":1,"location":{"type":"Point","coordinates":[31.247286,30.061209]},"textual_location":"امام مدخل شارع الجمهورية","advertiser_info":{},"meta":{},"created_at":"2022-08-31T19:49:34.109711Z","updated_at":"2022-08-31T19:49:34.109728Z"},{"id":21,"name":"امام عمارة رمسيس","status":1,"location":{"type":"Point","coordinates":[31.245902,30.06096]},"textual_location":"امام عمارة رمسيس","advertiser_info":{},"meta":{},"created_at":"2022-08-31T19:50:36.301748Z","updated_at":"2022-08-31T19:50:36.301767Z"},{"id":22,"name":"ش. الجمهورية مع نجيب الريحاني على رصيف المثلثة","status":1,"location":{"type":"Point","coordinates":[31.246648,30.055909]},"textual_location":"ش. الجمهورية مع نجيب الريحاني على رصيف المثلثة","advertiser_info":{},"meta":{},"created_at":"2022-08-31T19:51:54.274990Z","updated_at":"2022-08-31T19:51:54.275011Z"},{"id":23,"name":"امام صيدلية الاسعاف- شارع رمسيس","status":1,"location":{"type":"Point","coordinates":[31.238725,30.053738]},"textual_location":"امام صيدلية الاسعاف- شارع رمسيس","advertiser_info":{},"meta":{},"created_at":"2022-08-31T19:53:11.481737Z","updated_at":"2022-08-31T19:53:11.481758Z"},{"id":24,"name":"محطة مترو العتبة امام سور حديقة الازبكية","status":1,"location":{"type":"Point","coordinates":[31.246819,30.051955]},"textual_location":"محطة مترو العتبة امام سور حديقة الازبكية","advertiser_info":{},"meta":{},"created_at":"2022-08-31T19:54:29.278884Z","updated_at":"2022-08-31T19:54:29.278902Z"},{"id":25,"name":"ممر زكريا احمد من شارع ٢٦يوليو","status":1,"location":{"type":"Point","coordinates":[31.243497,30.052739]},"textual_location":"ممر زكريا احمد من شارع ٢٦يوليو","advertiser_info":{},"meta":{},"created_at":"2022-08-31T19:55:38.176651Z","updated_at":"2022-08-31T19:55:38.176669Z"}]
    setCoordinates(
        data.map((c) => {
          return {
            name: c.name,
            coordinates: {
              lon: c.location.coordinates[0],
              lat: c.location.coordinates[1],
            },
          };
        })
    );
    // axios
    //   .get(
    //     "http://ec2-18-144-101-126.us-west-1.compute.amazonaws.com:8000/api/stations/"
    //   )
    //   .then((res) => {
    //     setCoordinates(
    //       data.map((c) => {
    //         return {
    //           name: c.name,
    //           coordinates: {
    //             lon: c.location.coordinates[0],
    //             lat: c.location.coordinates[1],
    //           },
    //         };
    //       })
    //     );
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  }, []);

  // useEffect(() => {
  //   navigator.geolocation.getCurrentPosition((position) => {
  //     console.log("position", position.coords);
  //     setLongitude(position.coords.longitude);
  //     setLatitude(position.coords.latitude);
  //   });
  // }, []);
  console.log("langg", locale);
  let isArabic = locale !== "en";
  let textDirectionStyle = isArabic ? "flex-row-reverse" : "";
  return (
    <div className="container-fluid overflow-hidden">
      <div className="">
        <div className={`row ${textDirectionStyle}`}>
          <div className="col-md-4 station">
            <div className="station__heading">
              <h2>{t.googleMap.station}</h2>
              <img src="/images/map-pin.png" className="mt-5" width="250" />
            </div>
          </div>
          <div className="col-md-8">
            <div className="map">
              <Map data={coordinates} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default GoogleMap;

// GoogleApiWrapper({
//   apiKey: process.env.GOOGLE_API_KEY,
// })(GoogleMap);
// // export default GoogleMap
