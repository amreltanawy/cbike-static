import React from "react";
import { Accordion } from "react-bootstrap";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { useRouter } from "next/router";
import en from "./locales/en";
import fr from "./locales/fr";
const FaqArabic = () => {
  const router = useRouter();
  const { locale } = router;
  const t = locale === "en" ? en : fr;
  return (
    <div className="faqArabic container-fluid">
      <h1>{t.faq.question}</h1>
      <Accordion defaultActiveKey="0" className="faqArabic__accordian">
        {t.faqs.map((q, i) => (
          <Accordion.Item eventKey={i}>
            <Accordion.Header>{q.question}</Accordion.Header>
            <Accordion.Body>
              <ul
                style={{
                  textAlign: "right",
                  direction: "rtl",
                }}
              >
                {q.answer.map((point) => (
                  <li>{point}</li>
                ))}
              </ul>
            </Accordion.Body>
          </Accordion.Item>
        ))}
      </Accordion>
    </div>
  );
};

export default FaqArabic;
