import React from "react";
import { useRouter } from "next/router";
import en from "./locales/en";
import fr from "./locales/fr";
import styles from "../styles/Footer.module.scss";

const FooterArabic = () => {
  const router = useRouter();
  const { locale } = router;
  const t = locale === "en" ? en : fr;
  return (
    <div className="faq__footer container-fluid">
      <div className="row justify-content-around">
        <div className="col-md-3 faq__subscribe">
          <h4 className="text-right">{t.footer.subscribeNow}</h4>
          <div className="faq__footerbtn text-right">
            <a
              href="https://docs.google.com/forms/d/e/1FAIpQLSeIEACnSh25Gka5pCwkC5N7PtQXRop5L1iIvM2lpWpYr8ACYA/viewform"
              target="_blank"
            >
              <button className="btn btn-warning">{t.footer.submitBtn}</button>
            </a>
          </div>

        </div>{" "}
        <div
          className={`col-sm-2 ${styles.contactsCol}`}
          style={{
            textAlign: "right",
            direction: "rtl",
          }}
        >
          <h1 className="contacts-heading fs16 fs16m">اتصل بنا</h1>
          <div className="contacts fs16 fs14m">
            <p>
              التليفون: <a href="tel:+201033010081">+201033010081</a> -{" "}
              <a href="tel:+201019977738">+201019977738</a>
            </p>
            <p>ساعات العمل: الأحد-الخميس 9:00 صباحا - 5:00 مساءً </p>
          </div>
          <div className={styles.smLinks}>
            <a
              target="_blank"
              href="https://www.facebook.com/profile.php?id=100078219400438"
            >
              <img src="/images/fb.png" alt="fb" />
            </a>
            <a
              target="_blank"
              href="https://www.instagram.com/cairobikeeg/?hl=en"
            >
              <img src="/images/ig.png" alt="ig" />
            </a>
          </div>
        </div>
        <div className="col-sm-2 faq__logo">
          <img src="/images/website-logo.png" width="150" />
        </div>
      </div>
    </div>
  );
};

export default FooterArabic;
