import React, { useState, useEffect } from "react";
import {
  MapContainer,
  TileLayer,
  GeoJSON,
  useMap,
  Marker,
  Popup,
} from "react-leaflet";
// import geoData from '../map.geo.json';

export default (props) => {
  const { data } = props;
  const [onselect, setOnselect] = useState({});

  return (
    <MapContainer
      style={{ height: "100%", width: "100%", zIndex: 0 }}
      center={[30.033333, 31.233334]}
      zoom={12}
      scrollWheelZoom={false}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />

      {data &&
        data.map((item) => (
          <Marker position={[item.coordinates.lat, item.coordinates.lon]}>
            <Popup>{item.name}.</Popup>
          </Marker>
        ))}
    </MapContainer>
  );
};
