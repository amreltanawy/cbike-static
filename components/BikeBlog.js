import { useRouter } from "next/router";
import React from "react";

const BikeBlog = ({ image, title, description, date, author, link }) => {
  const router = useRouter();
  const { locale } = router;
  const isArabic = locale !== "en";
  const [isReadMore, setIsReadMore] = React.useState(false);
  return (
    <div
      className="col-md-6 blogList-content mb-2"
      style={isArabic ? { direction: "rtl", textAlign: "right" } : {}}
    >
      <img className="img-fluid blog-image" src={image} alt="nothing" />
      <br />
      <br />
      <a target="__blank" href={link}>
        <h3>{title}</h3>
      </a>
      <p>{isReadMore ? description : description.substring(0, 120)}</p>
      <span
        onClick={() => {
          setIsReadMore(!isReadMore);
        }}
        className="readmore-btn"
      >
        {isReadMore ? "Read less" : "Read more"}
      </span>
    </div>
  );
};

export default BikeBlog;
